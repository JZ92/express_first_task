import express from "express";
import fs from "fs/promises";
import path from "path";

const router = express.Router();

const readData = async (req, res, next) => {
  try {
    await fs.stat(path.resolve() + "/data/MOCK_USERS.json");

    // If the file exists, read it and set req.db as its data
    const users = await fs.readFile(
      path.resolve() + "/data/MOCK_USERS.json",
      "utf-8"
    );

    req.db = JSON.parse(users);
    next();
  } catch (err) {
    // If there is no such file, create an empty one and set req.db as an empty array
    console.log("Error: ", err);
    await fs.writeFile(
      path.resolve() + "/data/MOCK_USERSS.json",
      JSON.stringify([])
    );
    req.db = [];
    next();
  }
};

// Add the middleware to all the routes in "/users"
router.use(readData);

// GET all users
router.get("/getall", (req, res) => {
  res.status(200).json(req.db);
});

// GET an existing user
router.get("/getuserbyid/:user_id", (req, res) => {
  const user = req.db.find(
    (currUser) => Number(currUser.id) === Number(req.params.user_id)
  );
  user && res.status(200).json(user);

  !user &&
    res.status(404).send(`Could not find user number ${req.params.user_id}`);
});

// ADD a new user
router.post("/", async (req, res) => {
  const userToAdd = req.body;
  const newUsers = [...req.db, userToAdd];

  try {
    await fs.writeFile(
      path.resolve() + "/data/MOCK_USERS.json",
      JSON.stringify(newUsers)
    );
    res
      .status(200)
      .send(`Successfully added user with id number ${req.body.id}`);
  } catch (err) {
    throw err;
  }
});

// DELETE a user
router.delete("/delete/:user_id", async (req, res) => {
  const newUsers = req.db.filter(
    (currUser) => Number(currUser.id) !== Number(req.params.user_id)
  );

  if (newUsers.length === req.db.length)
    res
      .status(404)
      .send(`Could not find user number ${req.params.user_id} to delete`);

  try {
    await fs.writeFile(
      path.resolve() + "/data/MOCK_USERS.json",
      JSON.stringify(newUsers)
    );
    res
      .status(200)
      .send(`Successfully deleted user with id number ${req.params.user_id}`);
  } catch (err) {
    throw err;
  }
});

// PATCH - update a single field in an existing user
router.patch("/update_field/:user_id", async (req, res) => {
  const [key, value] = [Object.keys(req.body)[0], Object.values(req.body)[0]];

  let foundTheUser = false;

  const newUsers = req.db.map((currUser) => {
    if (Number(currUser.id) === Number(req.params.user_id)) {
      foundTheUser = true;
      currUser[key] = value;
    }
    return currUser;
  });

  !foundTheUser &&
    res
      .status(404)
      .send(`Could not find user number ${req.params.user_id} to update`);

  try {
    await fs.writeFile(
      path.resolve() + "/data/MOCK_USERS.json",
      JSON.stringify(newUsers)
    );
    res
      .status(200)
      .send(`Successfully update user with id number ${req.params.user_id}`);
  } catch (err) {
    throw err;
  }
});

// PUT - update a whole existing user
router.put("/replace/:user_id", async (req, res) => {
  let foundTheUser = false;

  const newUsers = req.db.map((currUser) => {
    if (Number(currUser.id) === Number(req.params.user_id)) {
      foundTheUser = true;
      return {
        id: Number(currUser.id),
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
      };
    } else return currUser;
  });

  !foundTheUser &&
    res
      .status(404)
      .send(`Could not find user number ${req.params.user_id} to replace`);

  try {
    await fs.writeFile(
      path.resolve() + "/data/MOCK_USERS.json",
      JSON.stringify(newUsers)
    );
    res
      .status(200)
      .send(`Successfully replaced user with id number ${req.params.user_id}`);
  } catch (err) {
    throw err;
  }
});

export default router;
