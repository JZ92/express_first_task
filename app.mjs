import express from "express";
import log from "@ajar/marker";
import morgan from "morgan";
import usersRouter from "./routers/usersRouter.mjs";
import winston from "winston";
import expressWinston from "express-winston";

const { PORT, HOST } = process.env;

const app = express();

app.use(morgan("dev"));

app.use(express.json());

app.use(
  expressWinston.logger({
    transports: [new winston.transports.Console()],
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.json()
    ),
    meta: true,
    msg: "HTTP {{req.method}} {{req.url}}",
    expressFormat: true,
    colorize: false,
    ignoreRoute: function (req, res) {
      return false;
    },
  })
);

app.use("/api/users", usersRouter);

app.get("/", (req, res) => {
  res.status(200).send("Hello Express!");
});

app.get("*", (req, res) => res.status(404).send("Wrong url! 404 error"));

app.listen(PORT, HOST, () => {
  log.magenta(`🌎  listening on`, `http://${HOST}:${PORT}`);
});
